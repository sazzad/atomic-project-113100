<?php

include_once ('../../../vendor/autoload.php');

use App\BITM\SEIP113100\Radio\Radio;

$obj = new Radio;
$obj = $obj->prepare($_GET)->show();
//var_dump($obj);

?>

<html>
    <head>
        <title>Gender</title>
        <style>
            body{width: 500px; margin: 0px auto; padding-top: 20px; font-family: "calibri";}
            table{width: 50%;}
            legend{font-size: 20px; font-weight: bold;  padding: 10px 5px;}
        </style>
    </head>
    
    <body>
        <table>
            <legend><u>Gender</u></legend>
            <tr>
                <td>ID</td>
                <td>: &nbsp;<?php echo $obj['id']?></td>
            </tr>
            <tr>
                <td>User Name</td>
                <td>: &nbsp;<?php echo $obj['user_name']?></td>
            </tr>
            <tr>
                <td>Gender</td>
                <td>: &nbsp;<?php echo $obj['gender']?></td>
            </tr>
        </table>
        <div style="padding-top: 50px;">
            <a href="create.php"><button>Create</button></a>&nbsp;&nbsp;
            <a href="index.php"><button>Gender List</button></a>&nbsp;&nbsp;
            <a href="../../../index.php"><button>Home</button></a>
            </div>
    </body>
</html>


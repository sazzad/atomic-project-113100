<?php

//var_dump($_GET);
//die();

include_once ('../../../vendor/autoload.php');
use App\BITM\SEIP113100\Hobby\Hobby;

$obj = new Hobby();
$_obj = $obj->prepare($_GET) ->edit();
//var_dump($_obj);

$data = $_obj['hobby'];
//var_dump($data);

$store  = array(explode(",", $data));

$data1 = array();

foreach($store[0] as $val){
    array_push($data1, trim($val));
}
//var_dump($store);
//echo '<pre>';
//print_r($data1);
//echo '</pre>';
?>


<html>
    <head>
        <title>Edit Hobby</title>
        <style>
           body{font-family: "calibri";}
            .content{width: 600px; margin: 50px auto;}
            table{height: 170px;}
            legend{font-size: 20px; font-weight: bold;  padding: 10px 5px;}
        </style>
    </head>
<body>

    <div class="content">
        <form action="update.php" method="POST">
        <fieldset>
            <legend>Edit Hobby</legend>
            <table>
                <tr>
                    <td><input type="hidden" name="id" value="<?php echo $_obj['id']?>" ></td>
                </tr>
                <tr>
                    <td>User Name:</td>
                    <td><input type="text" name="user_name" value="<?php echo $_obj['user_name']?>"></td>
                </tr>
                <tr>
                    <td>Hobby:</td>
                    <td>
                        <input type="checkbox" name="hobby[]" value="PHP" <?php if(in_array("PHP", $data1)) {echo 'checked';} else{echo '';}?>> PHP<br>
                        <input type="checkbox" name="hobby[]" value="C" <?php if(in_array("C", $data1)) {echo 'checked';} else{echo '';}?>> C<br>
                        <input type="checkbox" name="hobby[]" value="C++" <?php if(in_array("C++", $data1)) {echo 'checked';} else{echo '';}?>> C++<br>
                        <input type="checkbox" name="hobby[]" value="C#" <?php if(in_array("C#", $data1)) {echo 'checked';} else{echo '';}?>> C#
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td><input type="submit" value="Update"></td>
                </tr>
            </table>
        </fieldset>
      </form>
        <div style="padding-top: 20px;">
            <a href="create.php"><button>Create</button></a>&nbsp;&nbsp;
            <a href="index.php"><button>View All</button></a>&nbsp;&nbsp;
            <a href="../../../index.php"><button>Home</button></a>
        </div>
    </div><br>

</body>
</html>


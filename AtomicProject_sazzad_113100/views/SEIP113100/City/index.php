<?php
include_once ('../../../vendor/autoload.php');
use App\BITM\SEIP113100\City\Hobby;

$object = new Hobby();
$object= $object ->index();

?>


<html>
    <head>
        <title>Atomic Project</title>
        <style>
            body{font-family: "calibri";}
            .content{width: 600px; margin: 0px auto;}
            
            table{width: 100%; border: 1px solid #ccc; border-collapse: collapse;}
            table.hovertable{}
            table.hovertable tr{}
            table.hovertable tr th{ padding: 2px 5px; background: green; color: #fff; border: 1px solid #ccc; border-collapse: collapse;}
            table.hovertable tr td{ padding: 2px 5px; border: 1px solid #ccc; border-collapse: collapse;}
            table.hovertable tr:hover{}
        </style>
    </head>
    
    <body>
        <div class="content">
            <table class="hovertable">
                <caption><h3>View City</h3></caption>
                <tr>
                    <th>SL</th>
                    <th>ID</th>
                    <th>City</th>
                    <th><center>Action</center></th>
                </tr>
                
                <?php
                
                $sl = 0;
                foreach ($object as $object) {       
                    $sl ++;
                    ?>
                <tr>
                    <td><center><?php echo $sl?></center></td>
                    <td><center><?php echo $object['id']?></center></td>
                    <td><?php echo $object['name']?></td>
                    <td>
                    <center>
                        <a href="show.php?id=<?php echo $object['id']?> "><button>View</button></a>&nbsp;&nbsp;
                        <a href="edit.php?id=<?php echo $object['id']?> "><button>Edit</button></a>&nbsp;&nbsp;
                         <a href="trash.php?id=<?php echo $object['id']?> "><button>Trash</button></a>&nbsp;&nbsp;
                    </center>
                    </td>
                </tr>
                <?php
                
                }
                
                ?>
                
            </table>
            <div style="padding-top: 30px;">
                <a href="create.php"><button>Create</button></a>&nbsp;&nbsp;
                <a href="trashed.php"><button>Trashed Data</button></a>&nbsp;&nbsp;
            <a href="../../../index.php"><button>Home</button></a>
            </div>
        </div>
    </body>
</html>
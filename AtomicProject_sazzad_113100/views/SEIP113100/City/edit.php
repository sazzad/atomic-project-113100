<?php

//var_dump($_GET);
//die();

include_once ('../../../vendor/autoload.php');
use App\BITM\SEIP113100\City\Hobby;

$obj = new Hobby();
$_obj = $obj->prepare($_GET) ->edit();
//echo '<pre>';
//var_dump($_obj);
//echo '</pre>';

?>


<html>
    <head>
        <title>Edit Atomic Project</title>
        <style>
           body{font-family: "calibri";}
            .content{width: 600px; margin: 50px auto;}
            table{height: 100px;}
            legend{font-size: 20px; font-weight: bold;  padding: 10px 5px;}
        </style>
    </head>
<body>

    <div class="content">
        <form action="update.php" method="POST">
        <fieldset>
            <legend>Edit City</legend>
            <table>
                <tr>
                    <td><input type="hidden" name="id" value="<?php echo $_obj['id']?>" ></td>
                </tr>
                <tr>
                    <td>Hobby:</td>
                    <td>
                        <select name="name" style="width: 148px; height: 25px;">
                            <option value="Select One" <?php if(in_array("Select One", $_obj)) {echo 'selected';} else{echo '';}?>>Select One</option>
                            <option value="Dhaka" <?php if(in_array("Dhaka", $_obj)) {echo 'selected';} else{echo '';}?>>Dhaka</option>
                            <option value="Jhenaidah" <?php if(in_array("Jhenaidah", $_obj)) {echo 'selected';} else{echo '';}?>>Jhenaidah</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td><input type="submit" value="Update"></td>
                </tr>
            </table>
        </fieldset>
      </form>
        <div style="padding-top: 20px;">
            <a href="create.php"><button>Create</button></a>&nbsp;&nbsp;
            <a href="index.php"><button>View All</button></a>&nbsp;&nbsp;
            <a href="../../../index.php"><button>Home</button></a>
        </div>
    </div><br>

</body>
</html>


<?php

//var_dump($_GET);
//die();

include_once ('../../../vendor/autoload.php');
use App\BITM\SEIP113100\Book\Book;

$obj = new Book();
$_editData = $obj->prepare($_GET) ->edit();
//var_dump($_editData);

?>


<html>
    <head>
        <title>Atomic Project</title>
        <style>
            body{font-family: "calibri";}
            .content{width: 600px; margin: 0px auto;}
            table{height: 110px;}
        </style>
    </head>
<body>

    <div class="content">
        <form action="update.php" method="POST">
        <fieldset>
            <legend><h3>Edit Book Title</h3></legend>
            <table>
                <tr>
                    <td><input type="hidden" name="id" value="<?php echo $_editData['title_id']?>" required></td>
                </tr>
                <tr>
                    <td>First name:</td>
                    <td><input type="text" name="title" value="<?php echo $_editData['title']?>" required></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td><input type="submit" value="Update"></td>
                </tr>
            </table>
        </fieldset>
      </form>
        <div style="padding-top: 20px;">
            <a href="create.php"><button>Create</button></a>&nbsp;&nbsp;
            <a href="index.php"><button>View All</button></a>&nbsp;&nbsp;
            <a href="../../../index.php"><button>Home</button></a>
        </div>
    </div><br>

</body>
</html>


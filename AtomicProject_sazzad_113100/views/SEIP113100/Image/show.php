<?php

//echo "<pre>";
//var_dump($_POST);
//echo "</pre>";

include_once ('../../../vendor/autoload.php');

use App\BITM\SEIP113100\Book\Book;

$object2 = new Book;
//echo "<pre>";
//var_dump($object);
//echo "</pre>";
$value2 = $object2->prepare($_GET)->show();
//echo "<pre>";
//var_dump($value);
//echo "</pre>";


?>

<html>
    <head>
        <title>Book Title</title>
        <style>
            body{width: 600px; margin: 0px auto; padding-top: 20px; font-family: "calibri";}
            table{width: 50%;}
        </style>
    </head>
    
    <body>
    <legend><h3><u>Book Title</u></h3></legend>
        <table>
            <tr>
                <td>ID</td>
                <td>: &nbsp;<?php echo $value2['title_id']?></td>
            </tr>
            <tr>
                <td>Name</td>
                <td>: &nbsp;<?php echo $value2['title']?></td>
            </tr>
        </table>
        <div style="padding-top: 50px;">
            <a href="create.php"><button>Create</button></a>&nbsp;&nbsp;
            <a href="index.php"><button>View All</button></a>&nbsp;&nbsp;
            <a href="../../../index.php"><button>Home</button></a>
            </div>
    </body>
</html>


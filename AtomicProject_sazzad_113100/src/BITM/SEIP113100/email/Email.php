<?php

namespace App\BITM\SEIP113100\Email;
use App\BITM\SEIP113100\Email\Utility;
use PDO;

class Email{
    
    public $id = "";
    public $user_name = "";
    public $email = "";
    public $deleted_at = "";
    public $username = "root";
    public $password = "";    
    
    
    public function __construct(){
        $this->conn = new PDO('mysql:host=localhost; dbname=atomicproject_sazzad_113100', $this->username, $this->password);
        }
    
    
    public function prepare($data = array()){
        if(is_array($data) && array_key_exists('user_name', $data)){
            $this->user_name = $data['user_name'];
        }
        
         if(is_array($data) && array_key_exists('email', $data)){
            $this->email = $data['email'];
        }
        
        if (array_key_exists('id', $data) && !empty($data['id'])) {
            $this->id = $data['id'];
        }
        return $this;
    }
    
    
    
    public function store(){
        $query = "INSERT INTO `atomicproject_sazzad_113100`.`email` (`user_name`,`email`) VALUES (:user_name,:email)";
        $result = $this->conn->prepare($query);
        $result->execute(array(':user_name'=>  $this->user_name, ':email'=>  $this->email));
        
        if($result){
            Utility::redirectToCreate();
        }
        else{
             Utility::redirectToCreate();
        }
    }
    
    
    
   public function index(){
        $allData = array();
        $query = "SELECT * FROM atomicproject_sazzad_113100.email WHERE deleted_at IS NULL" ;
        $result = $this->conn->query( $query);
        $allData=$result->fetchAll(PDO::FETCH_ASSOC);
        return $allData;
    }
    
    
    
     public function show() {
        $query = "SELECT * FROM `atomicproject_sazzad_113100`.`email` WHERE `id`=:id";
        $result = $this->conn->prepare( $query);
        $result->execute(array(':id'=>$this->id));
        $row = $result->fetch(PDO::FETCH_ASSOC);
        return $row;
    }
    
    
    
    public function edit() {
        $query = "SELECT * FROM `atomicproject_sazzad_113100`.`email` WHERE `id`=:id";
        $result = $this->conn->prepare( $query);
        $result->execute(array(':id'=>$this->id));
        $row = $result->fetch(PDO::FETCH_ASSOC);
        return $row;
    }
    
    
    
    public function update(){
        $query = "UPDATE `atomicproject_sazzad_113100`.`email` SET `user_name`=:user_name,`email` =:email WHERE `email`.`id` = :id";
        //echo $query;
        $result = $this->conn->prepare($query);
        $result->execute(array(':id'=>$this->id,':user_name'=>$this->user_name,':email'=>$this->email));
        
        if($result){
            Utility::redirect_To_Index();
        }
        else{
             Utility::redirect_To_Index();
        }
    }
    
    
    
    public function trash(){
        $this->deleted_at = time() ;
        $query = "UPDATE `atomicproject_sazzad_113100`.`email` SET `deleted_at`=:deleted_at WHERE `email`.`id` = :id";
        //echo $query;
        $result = $this->conn->prepare($query);
        $result->execute(array(':id'=>$this->id,':deleted_at'=>$this->deleted_at));
        
        if($result){
            Utility::redirect_To_Index();
        }
        else{
             Utility::redirect_To_Index();
        }
    }
    
    
    
    public function trashed(){
        $allData = array();
        $query = "SELECT * FROM atomicproject_sazzad_113100.email WHERE deleted_at IS NOT NULL" ;
        $result = $this->conn->query( $query);
        $allData=$result->fetchAll(PDO::FETCH_ASSOC);
        return $allData;
    }
    
    
    
    
    public function recover(){
        $this->deleted_at =NULL;
        $query = "UPDATE `atomicproject_sazzad_113100`.`email` SET `deleted_at`=:deleted_at WHERE `email`.`id` = :id";
        //echo $query;
        $result = $this->conn->prepare($query);
        $result->execute(array(':id'=>$this->id,':deleted_at'=>$this->deleted_at));
        
        if($result){
            Utility::redirect_To_trashed();
        }
        else{
             Utility::redirect_To_trashed();
        }
    }
    
    
    
    public function delete(){
        $query = " DELETE FROM `atomicproject_sazzad_113100`.`email` WHERE `email`.`id` = :id";
        //echo $query;
        $result = $this->conn->prepare($query);
        $result->execute(array(':id'=>$this->id));
        
        if($result){
            Utility::redirect_To_trashed();
        }
        else{
             Utility::redirect_To_trashed();
        }
    }
    
    
    
}

?>


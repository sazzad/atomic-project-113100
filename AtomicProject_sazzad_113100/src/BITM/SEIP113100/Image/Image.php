<?php

namespace App\BITM\SEIP113100\Image;
namespace App\BITM\SEIP113100\Image\Message;
namespace App\BITM\SEIP113100\Image\Utility;
use PDO;


class Image {
    public $id = "";
    public $user_name = "";
    public $image = "";
    public $deleted_at = "";
    public $username = "root";
    public $password = "";
    public $conn = "";
    
    
    public function __construct() {
        $this->conn = new PDO('mysql:host=localhost;dbname=atomicProject_sazzad_113100', $this->user, $this->pass);
    }
    
    
    public function prepare($data = array()) {
            if (is_array($data) && array_key_exists('image', $data)) {
                $this->image = $data['image'];

             }

            if (array_key_exists('id', $data) && !empty($data['id'])) {
                $this->id = $data['id'];
            }
             if (array_key_exists('user_name', $data) && !empty($data['user_name'])) {
                $this->user_name = $data['user_name'];
            }

            return $this;
        }
    
    
    public function store() {
       
        if (!empty($this->image)) {
            $query = "INSERT INTO `atomicProject_sazzad_113100`.`image` (`user_name`,`image`) VALUES (:user_name,:image)";
            $result = $this->conn->prepare($query);
            $result->execute(array(':user_name'=>$this->username, ':image'=>$this->image));

//            if ($result) {
//                Message::message("Data has been stored successfully");
//                Utility::redirect();
//            }
        } 
//        else {
//            Utility::redirect();
//        }
    }
    
    
    
}
?>


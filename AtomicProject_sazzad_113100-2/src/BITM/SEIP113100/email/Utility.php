<?php
namespace App\BITM\SEIP113100\Email;

class Utility{
    public static function d($data=""){
        echo "<pre>";
        echo var_dump($data);
        echo "</pre>";
        
    }
    
    public static function dd($data=""){
        echo "<pre>";
        echo var_dump($data);
        echo "</pre>";
        die();
        
    }
    
    
    public static function redirectToCreate(){
        header('Location:create.php');
   }
   
   
    public static function redirect_To_Index(){
        header('Location:index.php');
   }
   
   
   public static function redirect_To_trashed(){
        header('Location:trashed.php');
   }
    
    
    
}
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Atomic Project</title>
        <style>
            body{font-family: "calibri";}
            .container{width: 600px; margin: 0px auto;}
            p{text-align: center; border-bottom-style: double; border-color: orange; font-weight: normal; font-size: 18px; padding-bottom: 5px;}
            .project-list{padding: 0px 20px; margin: 0px; list-style-type: decimal;}
            .project-list li{padding: 2px 10px;}
            .project-list li a{text-decoration: none; font-size: 18px;}
            .project-list li a:hover{color: brown;}
        </style>
    </head>
    <body>
        <div class="container">
            <p>Bangladesh Association of Software and Information Services (BASIS)<br>
                Md. Sazzad Hossain, SEIP: 113100</p>
            
            <h3>Atomic Project:</h3>
            <ul class="project-list">
                <li><a href="views/SEIP113100/Book/create.php">Book Title</a></li>
                <li><a href="views/SEIP113100/Birthday/create.php">Date: Birthday</a></li>
                <li><a href="views/SEIP113100/Textarea/create.php">Textarea: Summary of Organizations</a></li>
                <li><a href="views/SEIP113100/email/create.php">Email: Subscription (PDO)</a></li>
                <!--<li><a href="views/SEIP113100/Image/create.php">Profile Picture (PDO)</a></li>-->
                <li><a href="views/SEIP113100/Radio/create.php">Radio: Gender</a></li>
                <!--<li><a href="views/SEIP113100/Term/create.php">Checkbox (Single): Term & Condition</a></li>-->  
                <li><a href="views/SEIP113100/Hobby/create.php">Checkbox (Multiple): Hobby</a></li>                
                <li><a href="views/SEIP113100/city/create.php">Select (Single): City</a></li>
            </ul>
        </div>
    </body>
</html>

<?php

include_once ('../../../vendor/autoload.php');

use App\BITM\SEIP113100\Email\Email;

$object = new Email();

$value = $object->prepare($_GET)->show();
//echo "<pre>";
//var_dump($value);
//echo "</pre>";


?>

<html>
    <head>
        <title>Atomic Project</title>
        <style>
            body{width: 600px; margin: 0px auto; padding-top: 20px; font-family: "calibri";}
            table{width: 50%;}
            legend{font-size: 20px; font-weight: bold;  padding: 10px 5px;}
        </style>
    </head>
    
    <body>
    <legend>Email</legend>
        <table>
            <tr>
                <td>ID</td>
                <td>: &nbsp;<?php echo $value['id']?></td>
            </tr>
            
            <tr>
                <td>User name</td>
                <td>: &nbsp;<?php echo $value['user_name']?></td>
            </tr>
            
            <tr>
                <td>Email</td>
                <td>: &nbsp;<?php echo $value['email']?></td>
            </tr>
        </table>
        <div style="padding-top: 50px;">
            <a href="create.php"><button>Create</button></a>&nbsp;&nbsp;
            <a href="index.php"><button>View All</button></a>&nbsp;&nbsp;
            <a href="../../../index.php"><button>Home</button></a>
            </div>
    </body>
</html>


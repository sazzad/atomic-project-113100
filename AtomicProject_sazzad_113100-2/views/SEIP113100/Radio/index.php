<?php
include_once ('../../../vendor/autoload.php');
use App\BITM\SEIP113100\Radio\Radio;

$Gender = new Radio();
$_Gender = $Gender ->index();

?>


<html>
    <head>
        <title>Gender</title>
        <style>
            body{font-family: "calibri";}
            .content{width: 600px; margin: 0px auto;}
            
            table{width: 100%; border: 1px solid #ccc; border-collapse: collapse;}
            table.hovertable{}
            table.hovertable tr{}
            table.hovertable tr th{ padding: 2px 5px; background: green; color: #fff; border: 1px solid #ccc; border-collapse: collapse;}
            table.hovertable tr td{ padding: 2px 5px; border: 1px solid #ccc; border-collapse: collapse;}
            table.hovertable tr:hover{}
        </style>
    </head>
    
    <body>
        <div class="content">
            <table class="hovertable">
                <caption><h3>View Gender</h3></caption>
                <tr>
                    <th>SL</th>
                    <th>ID</th>
                    <th>User Name</th>
                    <th>Gender</th>
                    <th><center>Action</center></th>
                </tr>
                
                <?php
                
                $sl = 0;
                foreach ($_Gender as $gender) {       
                    $sl ++;
                    ?>
                <tr>
                    <td><center><?php echo $sl?></center></td>
                    <td><center><?php echo $gender['id']?></center></td>
                    <td><center><?php echo $gender['user_name']?></center></td>
                    <td><?php echo $gender['gender']?></td>
                    <td>
                    <center>
                        <a href="show.php?id=<?php echo $gender['id']?> "><button>View</button></a>&nbsp;&nbsp;
                        <a href="edit.php?id=<?php echo $gender['id']?> "><button>Edit</button></a>&nbsp;&nbsp;
                         <a href="trash.php?id=<?php echo $gender['id']?> "><button>Trash</button></a>&nbsp;&nbsp;
                    </center>
                    </td>
                </tr>
                <?php
                
                }
                
                ?>
                
            </table>
            <div style="padding-top: 30px;">
                <a href="create.php"><button>Create</button></a>&nbsp;&nbsp;
                <a href="trashed.php"><button>Trashed Data</button></a>&nbsp;&nbsp;
            <a href="../../../index.php"><button>Home</button></a>
            </div>
        </div>
    </body>
</html>
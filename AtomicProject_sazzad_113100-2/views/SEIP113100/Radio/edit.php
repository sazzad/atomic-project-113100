<?php

//var_dump($_GET);
//die();

include_once ('../../../vendor/autoload.php');
use App\BITM\SEIP113100\Radio\Radio;

$obj = new Radio();
$_obj = $obj->prepare($_GET) ->edit();
//var_dump($_obj);

?>


<html>
    <head>
        <title>Edit Gender</title>
        <style>
           body{font-family: "calibri";}
            .content{width: 600px; margin: 50px auto;}
            table{height: 150px;}
            legend{font-size: 20px; font-weight: bold;  padding: 10px 5px;}
        </style>
    </head>
<body>

    <div class="content">
        <form action="update.php" method="POST">
        <fieldset>
            <legend>Create Gender</legend>
            <table>
                <tr>
                    <td><input type="hidden" name="id" value="<?php echo $_obj['id']?>" ></td>
                </tr>
                <tr>
                    <td>User Name:</td>
                    <td><input type="text" name="user_name" value="<?php echo $_obj['user_name']?>"></td>
                </tr>
                <tr>
                    <td>Gender:</td>
                    <td>
                        <input type="radio" name="gender" value="Male" 
                            <?php if(preg_match("/Male/",$_obj['gender'])){ echo "Checked";} else { echo ""; }?>> Male<br>
                        
                        <input type="radio" name="gender" value="Female" 
                            <?php if(preg_match("/Female/",$_obj['gender'])){ echo "Checked";} else { echo ""; }?>> Female<br>
                        
                        <input type="radio" name="gender" value="Others" 
                            <?php if(preg_match("/Others/",$_obj['gender'])){ echo "Checked";} else { echo ""; }?>> Others<br>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td><input type="submit" value="Update"></td>
                </tr>
            </table>
        </fieldset>
      </form>
        <div style="padding-top: 20px;">
            <a href="create.php"><button>Create</button></a>&nbsp;&nbsp;
            <a href="index.php"><button>Gender List</button></a>&nbsp;&nbsp;
            <a href="../../../index.php"><button>Home</button></a>
        </div>
    </div><br>

</body>
</html>


<!DOCTYPE html>
<html>
    <head>
        <title>Gender</title>
        <style>
            body{font-family: "calibri";}
            .content{width: 600px; margin: 50px auto;}
            table{height: 150px;}
            legend{font-size: 20px; font-weight: bold;  padding: 10px 5px;}
        </style>
    </head>
<body>

    <div class="content">
        <form action="store.php" method="POST">
        <fieldset>
            <legend>Create Gender</legend>
            <table>
                <tr>
                    <td>User Name</td>
                    <td><input type="text" name="user_name" value="" required></td>
                </tr>
                <tr>
                    <td>Gender</td>
                    <td>
                        <input type="radio" name="gender" value="Male"> Male<br>
                        <input type="radio" name="gender" value="Female"> Female<br>
                        <input type="radio" name="gender" value="Others"> Others
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td><input type="submit" value="Submit"></td>
                </tr>
            </table>
        </fieldset>
      </form>
        <div style="padding-top: 20px;">
            <a href="index.php"><button>View All</button></a>&nbsp;&nbsp;
            <a href="../../../index.php"><button>Home</button></a>
        </div>
    </div><br>

</body>
</html>



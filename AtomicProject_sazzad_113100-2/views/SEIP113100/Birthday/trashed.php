<?php
include_once ('../../../vendor/autoload.php');
use App\BITM\SEIP113100\Birthday\Book;

$Book = new Book();
$_Book = $Book ->trashed();

?>


<html>
    <head>
        <title>Show Trashed Data</title>
        <style>
            body{font-family: "calibri";}
            .content{width: 600px; margin: 0px auto;}
            
            table{width: 100%; border: 1px solid #ccc; border-collapse: collapse;}
            table.hovertable{}
            table.hovertable tr{}
            table.hovertable tr th{ padding: 2px 5px; background: green; color: #fff; border: 1px solid #ccc; border-collapse: collapse;}
            table.hovertable tr td{ padding: 2px 5px; border: 1px solid #ccc; border-collapse: collapse;}
            table.hovertable tr:hover{}
        </style>
    </head>
    
    <body>
        <div class="content">
            <table class="hovertable">
                <caption><h3>Trashed Information</h3></caption>
                <tr>
                    <th>SL</th>
                    <th>ID</th>
                    <th>Birthday</th>
                    <th><center>Action</center></th>
                </tr>
                
                <?php
                
                $sl = 0;
                foreach ($_Book as $Book) {       
                    $sl ++;
                    ?>
                <tr>
                    <td><center><?php echo $sl?></center></td>
                    <td><center><?php echo $Book['id']?></center></td>
                    <td><?php echo $Book['b_date']?></td>
                    <td>
                    <center>
                        <a href="recover.php?id=<?php echo $Book['id']?> "><button>Restore</button></a>&nbsp;&nbsp;
                        <a onclick="return confirm('Are you sure delete this record?')" href="delete.php?id=<?php echo $Book['id']?> "><button>Delete</button></a>
                    </center>
                    </td>
                </tr>
                <?php
                
                }
                
                ?>
                
            </table>
            <div style="padding-top: 30px;">
                <a href="create.php"><button>Create</button></a>&nbsp;&nbsp;
                 <a href="index.php"><button>View All</button></a>&nbsp;&nbsp;
            <a href="../../../index.php"><button>Home</button></a>
            </div>
        </div>
    </body>
</html>
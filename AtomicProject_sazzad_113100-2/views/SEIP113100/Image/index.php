<?php
include_once ('../../../vendor/autoload.php');
use App\BITM\SEIP113100\Book\Book;

$obj = new Book();
$_Book = $obj ->index();

?>


<html>
    <head>
        <title>Book Title</title>
        <style>
            body{font-family: "calibri";}
            .content{width: 600px; margin: 0px auto;}
            
            table{width: 100%; border: 1px solid #ccc; border-collapse: collapse;}
            table.hovertable{}
            table.hovertable tr{}
            table.hovertable tr th{ padding: 2px 5px; background: green; color: #fff; border: 1px solid #ccc; border-collapse: collapse;}
            table.hovertable tr td{ padding: 2px 5px; border: 1px solid #ccc; border-collapse: collapse;}
            table.hovertable tr:hover{}
        </style>
    </head>
    
    <body>
        <div class="content">
            <table class="hovertable">
                <caption><h3>View Book Title</h3></caption>
                <tr>
                    <th>SL</th>
                    <th>ID</th>
                    <th>Book Title</th>
                    <th><center>Action</center></th>
                </tr>
                
                <?php
                
                $sl = 0;
                foreach ($_Book as $Book) {       
                    $sl ++;
                    ?>
                <tr>
                    <td><center><?php echo $sl?></center></td>
                    <td><center><?php echo $Book['title_id']?></center></td>
                    <td><?php echo $Book['title']?></td>
                    <td>
                    <center>
                        <a href="show.php?id=<?php echo $Book['title_id']?> "><button>View</button></a>&nbsp;&nbsp;
                        <a href="edit.php?id=<?php echo $Book['title_id']?> "><button>Edit</button></a>&nbsp;&nbsp;
                         <a href="trash.php?id=<?php echo $Book['title_id']?> "><button>Trash</button></a>&nbsp;&nbsp;
                    </center>
                    </td>
                </tr>
                <?php
                
                }
                
                ?>
                
            </table>
            <div style="padding-top: 30px;">
                <a href="create.php"><button>Create</button></a>&nbsp;&nbsp;
                <a href="trashed.php"><button>Trashed Data</button></a>&nbsp;&nbsp;
            <a href="../../../index.php"><button>Home</button></a>
            </div>
        </div>
    </body>
</html>